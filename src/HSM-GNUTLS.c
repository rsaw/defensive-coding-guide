#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <gnutls/gnutls.h>
#include <gnutls/abstract.h>
#include <gnutls/pkcs11.h>

//+ Features HSM-GNUTLS-PIN
int pin_function(void *userdata, int attempt, const char *token_url,
                 const char *token_label, unsigned flags, char *pin, size_t pin_max)
{
  if (flags & GNUTLS_PIN_FINAL_TRY)
    printf("This is the final try before locking!\n");
  if (flags & GNUTLS_PIN_COUNT_LOW)
    printf("Only few tries left before locking!\n");
  if (flags & GNUTLS_PIN_WRONG)
    printf("Wrong PIN has been provided in the previous attempt\n");

  /* userdata is the second value passed to gnutls_pkcs11_set_pin_function()
   * in this example we passed the PIN as a null terminated value.
   */
  snprintf(pin, pin_max, "%s", (char*)userdata);
  return 0;
}
//-

/* This program accepts on the command line:
 *  1. A PKCS#11 URL specifying a private key
 *  2. A PIN
 *  3. A PKCS#11 shared module (optional)
 *
 * And signs test data with the provided key.
 *
 * Example: ./a.out "pkcs11:object=myobject" 1234 /usr/lib64/pkcs11/opensc-pkcs11.so
 */
int main(int argc, char **argv)
{
  gnutls_privkey_t private_key;
  char *private_key_name;
  char *key_pass = NULL;
  const char *module_path = NULL;
  gnutls_datum_t testdata = {(void*)"TESTDATA", sizeof("TESTDATA")-1};
  gnutls_datum_t signature;
  int ret;

  if (argc < 2) {
    fprintf(stderr, "usage: %s [private key URL] [PIN] [module]\n", argv[0]);
    fprintf(stderr, "\n");
    exit(1);
  }

  private_key_name = argv[1];
  key_pass = argv[2];
  if (argc >= 3)
    module_path = argv[3];

  //+ Features HSM-GNUTLS
  if (module_path) {
    ret = gnutls_pkcs11_init(GNUTLS_PKCS11_FLAG_MANUAL, NULL);
    if (ret < 0) {
      fprintf(stderr, "error in %d: %s\n", __LINE__, gnutls_strerror(ret));
      exit(1);
    }

    ret = gnutls_pkcs11_add_provider(module_path, NULL);
    if (ret < 0) {
      fprintf(stderr, "error in %d: %s\n", __LINE__, gnutls_strerror(ret));
      exit(1);
    }
  }

  if (key_pass)
    gnutls_pkcs11_set_pin_function(pin_function, key_pass);

  ret = gnutls_privkey_init(&private_key);
  if (ret < 0) {
    fprintf(stderr, "error in %d: %s\n", __LINE__, gnutls_strerror(ret));
    exit(1);
  }

  ret = gnutls_privkey_import_url(private_key, private_key_name, 0);
  if (ret < 0) {
    fprintf(stderr, "error in %d: %s\n", __LINE__, gnutls_strerror(ret));
    exit(1);
  }

  ret = gnutls_privkey_sign_data(private_key, GNUTLS_DIG_SHA256, 0,
                                 &testdata, &signature);
  if (ret < 0) {
    fprintf(stderr, "error in %d: %s\n", __LINE__, gnutls_strerror(ret));
    exit(1);
  }

  gnutls_privkey_deinit(private_key);
  gnutls_free(signature.data);
  //-

  return 0;
}
